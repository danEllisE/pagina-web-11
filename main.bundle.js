webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = " <router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__article_list_article_list_component__ = __webpack_require__("./src/app/article-list/article-list.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(article) {
        article.articleOn();
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__article_list_article_list_component__["a" /* ArticleListComponent */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__article_list_article_list_component__["a" /* ArticleListComponent */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__article_article_component__ = __webpack_require__("./src/app/article/article.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__article_list_article_list_component__ = __webpack_require__("./src/app/article-list/article-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__contact_us_contact_us_component__ = __webpack_require__("./src/app/contact-us/contact-us.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__routing_routing_module__ = __webpack_require__("./src/app/routing/routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__console_console_component__ = __webpack_require__("./src/app/console/console.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__console_list_console_list_component__ = __webpack_require__("./src/app/console-list/console-list.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_4__article_article_component__["a" /* ArticleComponent */],
                __WEBPACK_IMPORTED_MODULE_5__article_list_article_list_component__["a" /* ArticleListComponent */],
                __WEBPACK_IMPORTED_MODULE_6__contact_us_contact_us_component__["a" /* ContactUsComponent */],
                __WEBPACK_IMPORTED_MODULE_8__console_console_component__["a" /* ConsoleComponent */],
                __WEBPACK_IMPORTED_MODULE_9__console_list_console_list_component__["a" /* ConsoleListComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_7__routing_routing_module__["a" /* AppRoutingModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]]
        }),
        __metadata("design:paramtypes", [])
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/article-list/article-list.component.html":
/***/ (function(module, exports) {

module.exports = "<section container-fluid>\n\t<div class=\"row\">\n\t\t<app-article class=\"col-6\" *ngFor=\" let article of articles\" [article]=\"article\"></app-article>\n\t</div>\n</section>"

/***/ }),

/***/ "./src/app/article-list/article-list.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/article-list/article-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArticleListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ArticleListComponent = /** @class */ (function () {
    function ArticleListComponent() {
        this.articles = [];
        this.articleOn();
    }
    ArticleListComponent.prototype.consoleOn = function () {
        this.articles = [];
        this.articles.push({
            type: 'console',
            id: 1,
            value: 0.9,
            title: 'PlayStation 4',
            img: '../assets/img/ps4.jpg',
            vid: '../assets/vid/ps4.mp4',
            body: "PlayStation 4 (oficialmente abreviada como PS4) es la cuarta videoconsola del modelo PlayStation Forma parte de las videoconsolas de octava generación. Fue anunciada oficialmente el 20 de febrero de 2013 en el evento PlayStation Meeting 2013, aunque el diseño de la consola no fue presentado hasta el 10 de junio en el E3 2013. Es la sucesora de laPlayStation 3 y compite con Wii U y Switch de Nintendo y Xbox One de Microsoft. Su lanzamiento fue el 15 de noviembre de 2013 en Estados Unidos y en Europa y Sudamérica fue el 29 de noviembre de 2013, mientras que en Japón fue el 22 de febrero de 2014.\n Alejándose de la compleja arquitectura utilizada en el procesador Cellde la videoconsola PlayStation 3, la PlayStation 4 cuenta con un procesador AMD de 8 núcleos bajo la arquitectura x86-64. Estas instrucciones x86-64 están diseñados para hacer más fácil el desarrollo de videojuegos en la consola de nueva generación, que atrae a un mayor número de desarrolladores. Estos cambios ponen de manifiesto el esfuerzo de Sony para mejorar las lecciones aprendidas durante el desarrollo, la producción y el lanzamiento de la PS3. Otras características de hardware notables de la PS4 es que incluyen 8 GB de memoria unificada GDDR5, una unidad de disco Blu-ray Disc más rápido, y los chips personalizados dedicados a tareas de procesamiento de audio, vídeo y de fondo.\n Entre las nuevas aplicaciones y servicios, Sony lanzó la aplicación PlayStation App, permitiendo a los que tengan un PS4 convertir losteléfonos inteligentes y las tabletas en una segunda pantalla para mejorar la jugabilidad o en teclados externos para más comodidad en el momento de escribir. La compañía también planeaba debutar con Gaikai, un servicio de juego basado en la nube que aloja contenidos y juegos descargables. Mediante la incorporación del botón 'Share' en el nuevo controlador hace que sea posible compartir en cualquier momento capturas de pantalla, trofeos, compras o videos en páginas como Facebook, Twitter y hacer stream de lo que se juegue y ver el de otros amigos en directo desde Ustream o Twitch, Sony planeó colocar más énfasis en el juego social. La consola PS4 el primer día de su lanzamiento vendió más de 1 millón de consolas sólo en territorio de los Estados Unidos.Al inicio de su conferencia de prensa en la Gamescom 2014, Sony anunció que ya había vendido más de 10 000 000 unidades de la PlayStation 4 en el mundo a usuarios finales. Está diseñada para la amplia integración con Playstation Vita.\n El Firmware actual de la consola es el 5.05, donde se mejora el rendimiento del sistema."
        }, {
            type: 'console',
            id: 2,
            value: 0.8,
            name: 'Xbox One',
            img: '../assets/img/xboxone.jpg',
            vid: '../assets/vid/xboxoone.webm',
            body: "Xbox One es la tercera videoconsola de sobremesa de la marca Xbox, producida por Microsoft. Forma parte de las videoconsolas de octava generación, fue presentada por Microsoft el 21 de mayo de 2013.3 Es la sucesora de la Xbox 360 y actualmente compite con PlayStation 4 de Sonyy Wii U y Switch de Nintendo. Su salida a la venta fue el 22 de noviembre de 20134 a un precio de 499 dólares.5\n\nXbox One cuenta con una GPU integrada basada en la tecnología de AMD y la equivalente a una HD 7790, 10 veces más potente que su Predecesora Xbox 360. Su GPU dispone de 768 Shaders y 1,31 TFLOPS/s de potencia. Puede mover varios millones de polígonos a la vez sin bajones de FPS, gracias a su procesador x86 de 8 núcleos con arquitectura similar a la de un PC. Microsoft había anunciado que la nueva API Direct X 12 llegaría a todas las GPU compatibles con Direct X 11.2 así mismo dejó claro que Xbox One era 100% compatible y que mediante una actualización llegaría a la consola. Esta nueva API reduce cálculos de la GPU en un máximo de un 20% y la CPU hasta un máximo de un 35% en algunos juegos, como por ejemplo, Forza Motorsport 5. Eso significa que los estudios disponen de un mayor rendimiento logrando unos gráficos aún más realistas. Así mismo Microsoft advirtió que estas cifras dependerían, no solo del tipo de juego, sino también del estudio que lo desarrolle. Junto con Direct X 12, Xbox One también dispone de la nube de Microsoft, la cual es capaz de hacer cálculos para liberar el procesamiento de las consolas."
        }, {
            type: 'console',
            id: 3,
            value: 0.95,
            name: 'Nintendo Switch',
            img: '../assets/img/switch.jpg',
            vid: '../assets/vid/switch.webm',
            body: "Nintendo Switch  es la séptima consola de videojuegos principal desarrollada por Nintendo. Conocida en el desarrollo por su nombre código «NX», se dio a conocer en octubre de 2016 y fue lanzada mundialmente el 3 de marzo de 2017.\nNintendo considera a Switch una consola híbrida. Se puede utilizar como consola de sobremesa con la unidad principal insertada en una estación de acoplamiento para conectarla con un televisor. Alternativamente, puede ser extraída de la base y utilizada de forma similar a una tableta a través de su pantalla táctil LCD, o colocada sobre una superficie gracias a su soporte integrado siendo así visible por varios jugadores.\nLa Switch utiliza dos controladores inalámbricos llamados en conjunto Joy-Con, que incluyen cuatro botones de acción estándar y un joystickdireccional, así como sensores para la detección de movimiento y retroalimentación táctil de alta definición, aunque se diferencian en algunos botones y características adicionales. Dos Joy-Con pueden conectarse uno a cada lado de la consola para usarse como consola portátil, conectarse al accesorio Grip proporcionado junto a la consola para usarlos como un mando más tradicional, o ser utilizados individualmente en la mano como elmando Wii, y de esta forma usarse con juegos multijugador locales."
        }, {
            type: 'console',
            id: 4,
            value: 0.7,
            name: 'Ps vita',
            img: '../assets/img/psvita.jpg',
            vid: '../assets/vid/psvita.mp4',
            body: "PlayStation Vita , también conocida como PS Vita, es una videoconsola portátil creada por Sony Computer Entertainment, es la sucesora de PlayStation Portable y es parte de la familia PlayStation de videoconsolas. Fue presentada el 27 de enero de 2011 y puesta a la venta el día 17 de diciembre del mismo año en Japón. La llegada a Europa y toda América fue el 22 de febrero de 2012. Inicialmente, estaba destinada a competir directamente con la consola deNintendo 3DS./nEl Firmware actual de la consola es el 3.67, donde se ha mejorado la calidad del rendimiento del sistema./nEl sistema presenta una forma ovalada similar al diseño de la primeraPSP y es la primera en tener dos joysticks, con una pantalla de cinco pulgadas OLED capacitiva localizada en el centro del sistema.7 Tiene dos palancas analógicas (una a cada lado de la pantalla), un pad, los cuatro botones clásicos de la PlayStation (, ,  y ), dos botones L y R, un botón PlayStation (Home), los botones Start y Select, Volumen +, Volumen - y Power. Por dentro el sistema presenta un procesador ARM Cortex-A9 MPCore de cuatro núcleos y un procesador gráfico SGX543MP4+. El sistema también trae un touchpad trasero, una cámara frontal, otra trasera, altavoces estéreo, micrófono, sistema sensor de movimiento SixAxis (giroscopio de tres ejes, acelerómetro de tres ejes), brújula electrónica de tres ejes, GPS, Wi-Fi, 3G, y Bluetoothv2.1+EDR.8"
        });
    };
    ArticleListComponent.prototype.articleOn = function () {
        this.articles = [];
        this.articles.push({
            type: 'article',
            id: 1,
            title: "Los videojuegos 3D podrían aumentar la capacidad de memoria",
            author: "Olympia",
            body: "En un estudio del 2015, publicado en The Journal of Neuroscience, reclutó a 69 participantes y pidieron a un tercero jugar a Super Mario 3D World durante dos semanas, un tercero a jugar a Angry Birds y el resto a no jugar nada.\n'Debido a sus experiencias atractivas y enriquecer los ambientes virtuales 3D, los mismos videojuegos que han sido jugados por décadas por niños y adultos por igual pueden realmente proporcionar a nuestro cerebro una estimulación significativa', escribieron los investigadores.\nLas personas que jugaron a Mario mejoraron en las tareas dememoria de seguimiento, mientras que las otras no mostraron mejoras antes y después del juego.",
            img: "none"
        }, {
            type: 'article',
            id: 2,
            title: "EL GRAN POTENCIAL DE LOS VIDEOJUEGOS ",
            author: "Olympia",
            body: "A partir del análisis de los casos, y extendiendo el concepto al resto de los videojuegos, vemos que hay factores positivos y factores negativos que conforman la estructura ideológica, los objetivos, las acciones, los valores y a su vez los potenciales resultados que los videojuegos pueden ofrecer a los jugadores. Pero lo que también se ve muy claramente es que no se trata solo de entretenimientos superficiales que entorpecen el libre pensar y accionar de las nuevas generaciones, como muchos creen. Se trata de una herramienta con capacidad de entretenimiento, que incluso tiene un gran potencial divulgador de conceptos de difícil tratamiento.",
            img: "none"
        }, {
            type: 'article',
            id: 3,
            title: "La Historia de los videojuegos",
            author: "Olympia",
            body: "La historia de los videojuegos tiene su origen en la década de 1940 cuando, tras el fin de la Segunda Guerra Mundial, las potencias vencedoras construyeron los primeros superordenadores programables como el ENIAC, de 1946.nota 1? Los primeros intentos por implementar programas de carácter lúdico (inicialmente programas de ajedrez) no tardaron en aparecer, y se fueron repitiendo durante las siguientes décadas.1? Los primeros videojuegos modernos aparecieron en la década de los 60, y desde entonces el mundo de los videojuegos no ha dejado de crecer y desarrollarse con el único límite que le ha impuesto la creatividad de losdesarrolladores y la evolución de la tecnología.2? En los últimos años, se asiste a una era de progreso tecnológico dominada por una industria que promueve un modelo de consumo rápido donde las nuevas superproducciones quedan obsoletas en pocos meses, pero donde a la vez un grupo de personas e instituciones -conscientes del papel que los programas pioneros, las compañías que definieron el mercado y los grandes visionarios tuvieron en el desarrollo de dicha industria- han iniciado el estudio formal de la historia de los videojuegos.",
            img: "none"
        }, {
            type: 'article',
            id: 4,
            title: "Los 10 juegos más vendidos de todos los tiempos",
            author: "Olympia",
            body: "none",
            img: "../assets/img/Lista.png"
        });
    };
    ArticleListComponent.prototype.ngOnInit = function () {
        this.articleOn();
    };
    ArticleListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-article-list',
            template: __webpack_require__("./src/app/article-list/article-list.component.html"),
            styles: [__webpack_require__("./src/app/article-list/article-list.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ArticleListComponent);
    return ArticleListComponent;
}());



/***/ }),

/***/ "./src/app/article/article.component.html":
/***/ (function(module, exports) {

module.exports = "\n<section class=\"card article\" >\n\t<h1 class=\"article-title\" *ngIf=\"article.title != 'none'\" >{{article.title}}</h1>\n\t<sub class=\"article-author\" *ngIf=\"article.author !='none'\" > {{article.author}}</sub>\n\t<hr>\n\t<img class=\"col-3\" *ngIf=\"article.img != 'none'\" src=\"{{article.img}}\">\n\t<video class=\"col-4\"*ngIf=\"article.vid != undefined\"src=\"{{article.vid}}\"></video>\n\t<article *ngIf=\"article.body != 'none'\" class=\"article-body\">\n\t\t{{article.body}}\n\t</article>\n</section>\n"

/***/ }),

/***/ "./src/app/article/article.component.scss":
/***/ (function(module, exports) {

module.exports = ".article {\n  margin-top: 3.5rem; }\n"

/***/ }),

/***/ "./src/app/article/article.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArticleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ArticleComponent = /** @class */ (function () {
    function ArticleComponent() {
    }
    ArticleComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ArticleComponent.prototype, "article", void 0);
    ArticleComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-article',
            template: __webpack_require__("./src/app/article/article.component.html"),
            styles: [__webpack_require__("./src/app/article/article.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ArticleComponent);
    return ArticleComponent;
}());



/***/ }),

/***/ "./src/app/console-list/console-list.component.html":
/***/ (function(module, exports) {

module.exports = "<section container-fluid>\n\t<div class=\"row\">\n\t\t<app-console class=\"col-6\" *ngFor=\" let console of consoles\" [console]=\"console\"></app-console>\n\t</div>\n</section>"

/***/ }),

/***/ "./src/app/console-list/console-list.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/console-list/console-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConsoleListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ConsoleListComponent = /** @class */ (function () {
    function ConsoleListComponent() {
        this.consoles = [{
                type: 'console',
                id: 1,
                value: 0.9,
                title: 'PlayStation 4',
                img: '../assets/img/ps4.jpg',
                vid: '../assets/vid/ps4.mp4',
                body: "PlayStation 4 (oficialmente abreviada como PS4) es la cuarta videoconsola del modelo PlayStation Forma parte de las videoconsolas de octava generación. Fue anunciada oficialmente el 20 de febrero de 2013 en el evento PlayStation Meeting 2013, aunque el diseño de la consola no fue presentado hasta el 10 de junio en el E3 2013. Es la sucesora de laPlayStation 3 y compite con Wii U y Switch de Nintendo y Xbox One de Microsoft. Su lanzamiento fue el 15 de noviembre de 2013 en Estados Unidos y en Europa y Sudamérica fue el 29 de noviembre de 2013, mientras que en Japón fue el 22 de febrero de 2014.\n Alejándose de la compleja arquitectura utilizada en el procesador Cellde la videoconsola PlayStation 3, la PlayStation 4 cuenta con un procesador AMD de 8 núcleos bajo la arquitectura x86-64. Estas instrucciones x86-64 están diseñados para hacer más fácil el desarrollo de videojuegos en la consola de nueva generación, que atrae a un mayor número de desarrolladores. Estos cambios ponen de manifiesto el esfuerzo de Sony para mejorar las lecciones aprendidas durante el desarrollo, la producción y el lanzamiento de la PS3. Otras características de hardware notables de la PS4 es que incluyen 8 GB de memoria unificada GDDR5, una unidad de disco Blu-ray Disc más rápido, y los chips personalizados dedicados a tareas de procesamiento de audio, vídeo y de fondo.\n Entre las nuevas aplicaciones y servicios, Sony lanzó la aplicación PlayStation App, permitiendo a los que tengan un PS4 convertir losteléfonos inteligentes y las tabletas en una segunda pantalla para mejorar la jugabilidad o en teclados externos para más comodidad en el momento de escribir. La compañía también planeaba debutar con Gaikai, un servicio de juego basado en la nube que aloja contenidos y juegos descargables. Mediante la incorporación del botón 'Share' en el nuevo controlador hace que sea posible compartir en cualquier momento capturas de pantalla, trofeos, compras o videos en páginas como Facebook, Twitter y hacer stream de lo que se juegue y ver el de otros amigos en directo desde Ustream o Twitch, Sony planeó colocar más énfasis en el juego social. La consola PS4 el primer día de su lanzamiento vendió más de 1 millón de consolas sólo en territorio de los Estados Unidos.Al inicio de su conferencia de prensa en la Gamescom 2014, Sony anunció que ya había vendido más de 10 000 000 unidades de la PlayStation 4 en el mundo a usuarios finales. Está diseñada para la amplia integración con Playstation Vita.\n El Firmware actual de la consola es el 5.05, donde se mejora el rendimiento del sistema."
            },
            {
                type: 'console',
                id: 2,
                value: 0.8,
                title: 'Xbox One',
                img: '../assets/img/xboxone.jpg',
                vid: '../assets/vid/xboxoone.webm',
                body: "Xbox One es la tercera videoconsola de sobremesa de la marca Xbox, producida por Microsoft. Forma parte de las videoconsolas de octava generación, fue presentada por Microsoft el 21 de mayo de 2013.3 Es la sucesora de la Xbox 360 y actualmente compite con PlayStation 4 de Sonyy Wii U y Switch de Nintendo. Su salida a la venta fue el 22 de noviembre de 20134 a un precio de 499 dólares.5\n\nXbox One cuenta con una GPU integrada basada en la tecnología de AMD y la equivalente a una HD 7790, 10 veces más potente que su Predecesora Xbox 360. Su GPU dispone de 768 Shaders y 1,31 TFLOPS/s de potencia. Puede mover varios millones de polígonos a la vez sin bajones de FPS, gracias a su procesador x86 de 8 núcleos con arquitectura similar a la de un PC. Microsoft había anunciado que la nueva API Direct X 12 llegaría a todas las GPU compatibles con Direct X 11.2 así mismo dejó claro que Xbox One era 100% compatible y que mediante una actualización llegaría a la consola. Esta nueva API reduce cálculos de la GPU en un máximo de un 20% y la CPU hasta un máximo de un 35% en algunos juegos, como por ejemplo, Forza Motorsport 5. Eso significa que los estudios disponen de un mayor rendimiento logrando unos gráficos aún más realistas. Así mismo Microsoft advirtió que estas cifras dependerían, no solo del tipo de juego, sino también del estudio que lo desarrolle. Junto con Direct X 12, Xbox One también dispone de la nube de Microsoft, la cual es capaz de hacer cálculos para liberar el procesamiento de las consolas."
            },
            {
                type: 'console',
                id: 3,
                value: 0.95,
                title: 'Nintendo Switch',
                img: '../assets/img/switch.jpg',
                vid: '../assets/vid/switch.webm',
                body: "Nintendo Switch  es la séptima consola de videojuegos principal desarrollada por Nintendo. Conocida en el desarrollo por su nombre código «NX», se dio a conocer en octubre de 2016 y fue lanzada mundialmente el 3 de marzo de 2017.\nNintendo considera a Switch una consola híbrida. Se puede utilizar como consola de sobremesa con la unidad principal insertada en una estación de acoplamiento para conectarla con un televisor. Alternativamente, puede ser extraída de la base y utilizada de forma similar a una tableta a través de su pantalla táctil LCD, o colocada sobre una superficie gracias a su soporte integrado siendo así visible por varios jugadores.\nLa Switch utiliza dos controladores inalámbricos llamados en conjunto Joy-Con, que incluyen cuatro botones de acción estándar y un joystickdireccional, así como sensores para la detección de movimiento y retroalimentación táctil de alta definición, aunque se diferencian en algunos botones y características adicionales. Dos Joy-Con pueden conectarse uno a cada lado de la consola para usarse como consola portátil, conectarse al accesorio Grip proporcionado junto a la consola para usarlos como un mando más tradicional, o ser utilizados individualmente en la mano como elmando Wii, y de esta forma usarse con juegos multijugador locales."
            },
            {
                type: 'console',
                id: 4,
                value: 0.7,
                title: 'Ps vita',
                img: '../assets/img/psvita.jpg',
                vid: '../assets/vid/psvita.mp4',
                body: "PlayStation Vita , también conocida como PS Vita, es una videoconsola portátil creada por Sony Computer Entertainment, es la sucesora de PlayStation Portable y es parte de la familia PlayStation de videoconsolas. Fue presentada el 27 de enero de 2011 y puesta a la venta el día 17 de diciembre del mismo año en Japón. La llegada a Europa y toda América fue el 22 de febrero de 2012. Inicialmente, estaba destinada a competir directamente con la consola deNintendo 3DS./nEl Firmware actual de la consola es el 3.67, donde se ha mejorado la calidad del rendimiento del sistema./nEl sistema presenta una forma ovalada similar al diseño de la primeraPSP y es la primera en tener dos joysticks, con una pantalla de cinco pulgadas OLED capacitiva localizada en el centro del sistema.7 Tiene dos palancas analógicas (una a cada lado de la pantalla), un pad, los cuatro botones clásicos de la PlayStation (, ,  y ), dos botones L y R, un botón PlayStation (Home), los botones Start y Select, Volumen +, Volumen - y Power. Por dentro el sistema presenta un procesador ARM Cortex-A9 MPCore de cuatro núcleos y un procesador gráfico SGX543MP4+. El sistema también trae un touchpad trasero, una cámara frontal, otra trasera, altavoces estéreo, micrófono, sistema sensor de movimiento SixAxis (giroscopio de tres ejes, acelerómetro de tres ejes), brújula electrónica de tres ejes, GPS, Wi-Fi, 3G, y Bluetoothv2.1+EDR.8"
            }
        ];
    }
    ConsoleListComponent.prototype.ngOnInit = function () {
    };
    ConsoleListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-console-list',
            template: __webpack_require__("./src/app/console-list/console-list.component.html"),
            styles: [__webpack_require__("./src/app/console-list/console-list.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ConsoleListComponent);
    return ConsoleListComponent;
}());



/***/ }),

/***/ "./src/app/console/console.component.html":
/***/ (function(module, exports) {

module.exports = "\n<section class=\"card article\" >\n\t<h1 class=\"article-title\" *ngIf=\"console.title != 'none'\" >{{console.title}}</h1>\n\t<sub>Calificación {{console.value * 100}}%</sub>\n\t<hr>\n\t<div class=\"row\">\n\t<img class=\"col-5\"*ngIf=\"console.img != 'none'\" src=\"{{console.img}}\">\n\t<video class=\"col-7\"*ngIf=\"console.vid != 'none'\" src=\"{{console.vid}}\"></video>\n</div>\n\t<article *ngIf=\"console.body != 'none'\" style=\"display:inline-block;\">\n\t\t{{console.body}}\n\t</article>\n</section>\n"

/***/ }),

/***/ "./src/app/console/console.component.scss":
/***/ (function(module, exports) {

module.exports = ".article {\n  margin-top: 3.5rem; }\n"

/***/ }),

/***/ "./src/app/console/console.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConsoleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ConsoleComponent = /** @class */ (function () {
    function ConsoleComponent() {
    }
    ConsoleComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ConsoleComponent.prototype, "console", void 0);
    ConsoleComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-console',
            template: __webpack_require__("./src/app/console/console.component.html"),
            styles: [__webpack_require__("./src/app/console/console.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ConsoleComponent);
    return ConsoleComponent;
}());



/***/ }),

/***/ "./src/app/contact-us/animation.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return slideInDownAnimation; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");

// Component transition animations
var slideInDownAnimation = Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* trigger */])('routeAnimation', [
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_20" /* state */])('*', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_21" /* style */])({
        opacity: 1,
        transform: 'translateX(0)'
    })),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_22" /* transition */])(':enter', [
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_21" /* style */])({
            opacity: 0,
            transform: 'translateX(-100%)'
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* animate */])('0.2s ease-in')
    ]),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_22" /* transition */])(':leave', [
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* animate */])('0.5s ease-out', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_21" /* style */])({
            opacity: 0,
            transform: 'translateY(100%)'
        }))
    ])
]);


/***/ }),

/***/ "./src/app/contact-us/contact-us.component.html":
/***/ (function(module, exports) {

module.exports = "<h3>Contactanos</h3>\n<div *ngIf=\"details\">\n  {{ details }}\n</div>\n<div>\n  <div>\n    <label>Mensaje: </label>\n  </div>\n  <div>\n    <textarea [(ngModel)]=\"message\" rows=\"10\" cols=\"35\" [disabled]=\"sending\"></textarea>\n  </div>\n</div>\n<p *ngIf=\"!sending\">\n  <button (click)=\"send()\">Enviar</button>\n  <button (click)=\"cancel()\">Cancelar</button>\n</p>\n\n<footer> Hecho por Dan Ellis Echavarria y Leiner</footer>\n"

/***/ }),

/***/ "./src/app/contact-us/contact-us.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/contact-us/contact-us.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactUsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_us_animation__ = __webpack_require__("./src/app/contact-us/animation.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContactUsComponent = /** @class */ (function () {
    function ContactUsComponent(router) {
        this.router = router;
        this.routeAnimation = true;
        this.display = 'block';
        this.position = 'absolute';
        this.sending = false;
    }
    ContactUsComponent.prototype.send = function () {
        var _this = this;
        this.sending = true;
        this.details = 'Enviando...';
        setTimeout(function () {
            _this.sending = false;
            _this.closePopup();
        }, 1000);
    };
    ContactUsComponent.prototype.cancel = function () {
        this.closePopup();
    };
    ContactUsComponent.prototype.closePopup = function () {
        // Providing a `null` value to the named outlet
        // clears the contents of the named outlet
        this.router.navigate([{ outlets: { popup: null } }]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* HostBinding */])('@routeAnimation'),
        __metadata("design:type", Object)
    ], ContactUsComponent.prototype, "routeAnimation", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* HostBinding */])('style.display'),
        __metadata("design:type", Object)
    ], ContactUsComponent.prototype, "display", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* HostBinding */])('style.position'),
        __metadata("design:type", Object)
    ], ContactUsComponent.prototype, "position", void 0);
    ContactUsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-contact-us',
            template: __webpack_require__("./src/app/contact-us/contact-us.component.html"),
            styles: [__webpack_require__("./src/app/contact-us/contact-us.component.scss")],
            host: { 'class': 'col-4' },
            animations: [__WEBPACK_IMPORTED_MODULE_2__contact_us_animation__["a" /* slideInDownAnimation */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]])
    ], ContactUsComponent);
    return ContactUsComponent;
}());



/***/ }),

/***/ "./src/app/routing/routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__article_list_article_list_component__ = __webpack_require__("./src/app/article-list/article-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_us_contact_us_component__ = __webpack_require__("./src/app/contact-us/contact-us.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__console_list_console_list_component__ = __webpack_require__("./src/app/console-list/console-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_1__article_list_article_list_component__["a" /* ArticleListComponent */], },
    { path: 'articles', component: __WEBPACK_IMPORTED_MODULE_1__article_list_article_list_component__["a" /* ArticleListComponent */] },
    { path: 'about', component: __WEBPACK_IMPORTED_MODULE_2__contact_us_contact_us_component__["a" /* ContactUsComponent */] },
    { path: 'console', component: __WEBPACK_IMPORTED_MODULE_3__console_list_console_list_component__["a" /* ConsoleListComponent */] },
    { path: '**', redirectTo: '', pathMatch: 'full' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* RouterModule */].forRoot(appRoutes, {
                    enableTracing: true,
                })
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* RouterModule */]
            ],
            providers: []
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map